package secondweekpractice;

import java.util.Scanner;

/*
Считать данные из консоли о типе номера отеля.
1 - VIP, 2 - PREMIUM, 3 - STANDART
Вывести цену номера VIP - 125, PREMIUM - 110, STANDART - 100.
 */
public class Task4 {
    public static final int VIP_PRICE = 125;
    public static final int PREMIUM_PRICE = 120;
    public static final int STANDART_PRICE = 100;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите номер отеля от 1 до 3");
        int roomType = scanner.nextInt();

//        if (roomType == 1) {
//            System.out.println("ViP Price" + VIP_PRICE);
//
//        }
//        if (roomType == 2) {
//            System.out.println("Premium Price" + PREMIUM_PRICE);
//        }
//        if (roomType == 3) {
//            System.out.println("Standart Price" + STANDART_PRICE);
//        } else {
//            System.out.println("введите корректный номер");
//        }

        switch (roomType) {
            case 1:
                System.out.println("ViP Price" + VIP_PRICE);
                break;
            case 2:
                System.out.println("Premium Price" + PREMIUM_PRICE);
                break;
            case 3:
                System.out.println("Standart Price" + STANDART_PRICE);
                break;
            default:
                System.out.println("введите корректный номер");
                break;
        }
    }
}
