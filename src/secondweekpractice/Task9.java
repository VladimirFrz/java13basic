package secondweekpractice;

import java.util.Scanner;

/*
Дана строка в патерн, заменять на паттерн, состоящий из заглавных символов
Входные данные:
Hello
o
Выходные данные:
HellO

Входные данные
Hello world
ld
Выходные данные
Hello worLD
 */
public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        String pattern = scanner.next();

        String upperPatern = pattern.toUpperCase();
        System.out.println(str.replace(pattern, upperPatern));

    }

}
